alter table contract_service_office add staff_id int not null;
alter table contract_service_office add foreign key (staff_id) references staff(id);