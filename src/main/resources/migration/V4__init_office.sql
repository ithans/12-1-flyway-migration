create table offices(
    id int not null,
    country varchar(128) not null,
    city varchar(128) not null,
    primary key (id)
)