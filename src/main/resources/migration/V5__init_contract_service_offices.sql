create table contract_service_office(
    office_id int not null,
    contract_id int not null ,
    primary key (office_id,contract_id),
    foreign key (office_id) references offices(id),
    foreign key (contract_id) references contract(id)
)