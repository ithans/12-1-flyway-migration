create table client(
    id int not null ,
    full_name varchar(128) not null ,
    abbreviation varchar(6) not null,
    primary key (id)
)