create table staff(
    id int not null,
    firstName varchar(64) not null,
    lastName varchar(64) not null,
    office_id int not null,
    primary key (id),
    foreign key (office_id) references offices(id)
)