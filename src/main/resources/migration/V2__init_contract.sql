create table contract(
    id int not null,
    name varchar(32) not null,
    client_id int not null,
    primary key (id),
    foreign key (client_id) references client(id)
)